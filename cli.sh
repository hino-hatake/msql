#!/bin/sh

export $(cat .env | grep ^[A-Z] | xargs)

docker-compose exec --user root mysql mysql \
  --user=root \
  --password="$MYSQL_ROOT_PASSWORD" \
  --database=mysql \
  -e "$*;"

