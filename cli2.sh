#!/bin/sh

export $(cat .env | grep ^[A-Z] | xargs)

# docker run --rm -it --network mysql_my-net \
docker run --rm -it --network container:mysql \
  mysql:8.0.24 mysql \
  --host=mysql \
  --database=$MYSQL_DATABASE \
  --user=$MYSQL_USER \
  --password=$MYSQL_PASSWORD \
  --execute="$*;"

