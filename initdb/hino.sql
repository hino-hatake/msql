CREATE USER IF NOT EXISTS 'hino'@'%' IDENTIFIED BY 'hino' ATTRIBUTE '{"comment" : "Do not reveal my rank!", "rank": "demigod"}';

CREATE DATABASE IF NOT EXISTS hino DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_general_ci;

GRANT ALL ON hino.* TO 'hino'@'%';

