## Just some tips
### 1. DBeaver 21.0
if
```
SQL Error [08001]: Public Key Retrieval is not allowed
```
then `Connection Settings` > `Driver properties` > `User Properties` > `usessl = false`

### 2. mysql-cli 8.0
```sh
./cli.sh select version\(\)
./cli.sh show databases
./cli.sh select current_timestamp
./cli.sh select unix_timestamp\(\)
./cli.sh 'use mysql;select user, host from user'
```
